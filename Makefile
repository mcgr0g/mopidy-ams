MAINTAINER=mcgr0g
ASSEMBLY_NAME=mopidy-ams
IMG_NAME=$(MAINTAINER)/$(ASSEMBLY_NAME)
# VERSIONS ---------------------------------------------------------------------
CORE_LANG_VER=3.12
ASSEMBLY_VER=0.0.02
ASSEMBLY_DATE:=$(shell date '+%Y-%m-%d')


## https://github.com/docker-library/python/blob/1b7a1106674a21e699b155cbd53bf39387284cca/3.12/bookworm/Dockerfile
# BASE_IMG=python:3.12.2-bookworm

## https://github.com/linuxserver/docker-python/blob/main/Dockerfile
# BASE_IMG=linuxserver/python:3.12.2

## https://github.com/docker-library/python/blob/1b7a1106674a21e699b155cbd53bf39387284cca/3.12/slim-bookworm/Dockerfile
BASE_IMG=python:3.12.2-slim-bookworm

# BUILD FLAGS -----------------------------------------------------------------

BFLAGS=docker build \
		--build-arg assembly_ver=$(ASSEMBLY_VER) \
		--build-arg assembly_date=$(ASSEMBLY_DATE) \
		--build-arg assembly_name=$(ASSEMBLY_NAME) \
		--build-arg base_img=$(BASE_IMG) \
		--build-arg core_lang_ver=$(CORE_LANG_VER) \
		-t $(IMG_NAME):$(ASSEMBLY_VER)

BUILD_FAST=$(BFLAGS) .
BUILD_FULL=$(BFLAGS) --no-cache .
UPGRAGE_PKGS=$(BFLAGS)  --build-arg UPGRADE=true .
UPDATE_PKGS=$(BFLAGS)  --build-arg UPDATE=true .
RECONF=$(BFLAGS) --progress=plain --build-arg RECONFIGURED=true .

# IMAGE -----------------------------------------------------------------------

build:
	$(BUILD_FAST)
	
build-full:
	$(BUILD_FULL)

upgrade:
	$(UPGRAGE_PKGS)

update:
	$(UPDATE_PKGS)

reconf:
	$(RECONF)

login:
	docker login

prepush:
	docker tag $(IMG_NAME):$(ASSEMBLY_VER) $(IMG_NAME):latest

push:
	docker push $(IMG_NAME) --all-tags

pull:
	docker pull $(IMG_NAME)

# RUN FLAGS -------------------------------------------------------------------


IMITROOTLESS=mkdir ${PWD}/cache && mkdir ${PWD}/config

RUNBASE=docker run --rm --name $(ASSEMBLY_NAME) \
		-u ${shell id -u}:${shell id -g} \
		-v "${PWD}/media:/data/music:ro" \
		-v ${PWD}/cache:/app_data \
		-p 6680:6680 \
		-t $(IMG_NAME):$(ASSEMBLY_VER)

RUN=$(RUNBASE) .
RUN_SCAN=$(RUNBASE) mopidy local scan
RUN_V=$(RUNBASE) mopidy -v

# CONTAINER -------------------------------------------------------------------

# имитация docker rootless
# важно прокидивать каталог с неруторвыми правами доступа, что бы в этрипоинте не падало копирование с нехваткой прав
volumes:
	- mkdir ${PWD}/cache && mkdir ${PWD}/config

scan: clear volumes
	$(RUN_SCAN)

run: volumes
	$(RUN)

debug: clear volumes
	$(RUN_V)

flop:
	docker exec -it --user mopidy $(ASSEMBLY_NAME) /bin/bash

clear:
	rm -rf cache/
	rm -rf config/