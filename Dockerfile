ARG base_img
FROM $base_img
ARG assembly_ver
ARG core_lang_ver
ARG assembly_name
ARG assembly_date=$(date +%Y-%m-%d)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # 
ARG UPGRADE=false
RUN set -ex \
 && apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y\
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
      gir1.2-gst-plugins-base-1.0 \
      gir1.2-gstreamer-1.0 \
      gstreamer1.0-plugins-good \
      gstreamer1.0-plugins-ugly \
      gstreamer1.0-plugins-ugly \
      gstreamer1.0-tools \
      libcairo2-dev \
      libxt-dev \
      libgirepository1.0-dev \
      python3-gst-1.0 \
      curl \
      python3-cryptography \
      python3-distutils \
      dumb-init \
      git \
      sudo \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache

# # # # # # # # # # # # # # # # # # # # # # # # # # # # 
ARG UPDATE=false

COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir --break-system-packages -r requirements.txt 

ENV docker_user=mopidy
# https://github.com/jaedb/Iris/wiki/Getting-started#to-install-using-pip
RUN sh -c 'echo "${docker_user}  ALL=NOPASSWD:   /usr/local/lib/python${core_lang_ver}/site-packages/mopidy_iris/system.sh" >> /etc/sudoers'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # 
ARG RECONFIGURED=false
ENV CONFIG=/config \
  APP_DATA=/app_data \
  USR_DATA=/data \
  SETUP=/setup \
  HOME=/home/$docker_user

COPY /entrypoint.sh /entrypoint.sh
COPY $SETUP/ $SETUP/

RUN set -ex \
&& echo "1" >> /IS_CONTAINER \
&& useradd --create-home $docker_user \
&& usermod -G audio,sudo $docker_user \
&& mkdir -p \
  $HOME \
  $CONFIG/mopidy \
  $APP_DATA/data \
  $APP_DATA/cache \
  $USR_DATA/music \
  $USR_DATA/m3u \
&& cp $SETUP/pulse-client.conf $CONFIG \
&& cp $SETUP/mopidy.conf $CONFIG/mopidy \
&& ln -sf $CONFIG/mopidy/mopidy.conf $CONFIG/mopidy.conf \
&& ln -sf $CONFIG/pulse-client.conf /etc/pulse/client.conf \
&& chown -R $docker_user:audio \
  $HOME \
  $SETUP \
  $CONFIG \
  $APP_DATA \
  $USR_DATA \
  /entrypoint.sh \
&& chmod -R go+rwx \
  $HOME \
  $SETUP \
  $CONFIG \
  $APP_DATA \
  $USR_DATA \
  /entrypoint.sh

WORKDIR $CONFIG

ENV XDG_CONFIG_HOME=$CONFIG \
  XDG_DATA_HOME=$APP_DATA/data \
  XDG_CACHE_HOME=$APP_DATA/cache \
  XDG_MUSIC_HOME=$USR_DATA \
  IRIS_CONFIG_LOCATION=$CONFIG/mopidy/mopidy.conf \
  IRIS_USE_SUDO=false

USER $docker_user
ENTRYPOINT ["/usr/bin/dumb-init", "/entrypoint.sh"]
CMD ["bash", "-c", "mopidy"]

EXPOSE 6600 6680

VOLUME /config /app_data /usr_data

HEALTHCHECK --interval=5s --timeout=2s --retries=20 \
    CMD curl --connect-timeout 5 --silent --show-error --fail http://localhost:6680/ || exit 1

LABEL org.opencontainers.image.authors="Ronnie McGrog" \
    org.opencontainers.image.url="https://hub.docker.com/repository/docker/mcgr0g/mopidy-ams/general" \
    org.opencontainers.image.documentation="https://gitlab.com/mcgr0g/mopidy-ams/README.md" \
    org.opencontainers.image.source="https://gitlab.com/mcgr0g/mopidy-ams" \
    org.opencontainers.image.title="${assembly_name}" \
    org.opencontainers.image.description="mopidy assembly with several online sources" \
    org.opencontainers.image.version="${assembly_ver}" \
    org.opencontainers.image.created="${assembly_date}"