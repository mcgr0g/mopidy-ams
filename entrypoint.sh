#!/bin/bash

# hack for mounting config config folder to host os with custom user IN and OUT of container
mkdir -p \
  $CONFIG/mopidy/

# copy config unless it exists
cp -n $SETUP/mopidy.conf $CONFIG/mopidy/mopidy.conf
# hack for iris webui local scan command
ln -s $CONFIG/mopidy/mopidy.conf $CONFIG/mopidy.conf

cp -n $SETUP/pulse-client.conf $CONFIG/pulse-client.conf

if [ -z "$PULSE_COOKIE_DATA" ]
then
    echo -ne $(echo $PULSE_COOKIE_DATA | sed -e 's/../\\x&/g') >$HOME/pulse.cookie
    export PULSE_COOKIE=$HOME/pulse.cookie
fi

exec "$@"
