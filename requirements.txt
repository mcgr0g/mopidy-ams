# https://pypi.org/project/PyGObject/#history
PyGObject==3.48.2

# https://github.com/mopidy/mopidy/releases
Mopidy==3.4.2

# https://github.com/jaedb/Iris/releases
Mopidy-Iris==3.69.3

# https://github.com/mopidy/mopidy-local/releases
Mopidy-Local==3.2.1

# original https://pypi.org/project/mopidy-yamusic/#history
# fork with fixes https://github.com/obsqrbtz/mopidy-yamusic
Mopidy-YaMusic @ git+https://github.com/obsqrbtz/mopidy-yamusic.git@676090b

# https://github.com/mopidy/mopidy-mpd/releases
Mopidy-MPD==3.3.0

# https://github.com/tkem/mopidy-internetarchive/tags
Mopidy-InternetArchive==3.0.1

# https://github.com/tkem/mopidy-mobile/tags
Mopidy-Mobile==1.10.0

# prerequisite for Mopidy-Youtube
yt-dlp==2024.7.9

# https://github.com/natumbri/mopidy-youtube/releases
Mopidy-Youtube==3.7