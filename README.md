# mopidy-ams
[mopidy](https://mopidy.com/) with alternative music sources

## usage
```yaml
name: mopidy-ams
services:
  mopidy:
    container_name: mopidy-ams
    image: mcgro0g/mopidy-ams:latest
    user: ${PUID}:${PGID}     # set owner of music files
    environment:
      TZ: ${TZ}
    volumes:
      - ./mopidy/config:/config
      - ./mopidy/m3u:/data/m3u  # To persist local playlists
      - ${MUSIC}:/data/music:ro # local media files
      - ${MUSIC2}:/torrents:ro      # media files from torrents
      - ./mopidy/app_data:/app_data # To persist app data for lyrics and etc
      - /tmp/audio-fifos/snapcast_fifo:/tmp/snapcast_fifo # snapcast integeation https://github.com/badaix/snapcast?tab=readme-ov-file#setup-of-audio-playersserver
    ports:
      - 6600:6600 # MPD server. For ncmpcpp client
      - 6680:6680 # HTTP server. For browser-client
    restart: unless-stopped
    network_mode: host
```

## docs
- [EN](https://gitlab.com/mcgr0g/mopidy-ams/-/blob/main/docs/en/00-index.adoc) 
- [RU](https://gitlab.com/mcgr0g/mopidy-ams/-/blob/main/docs/ru/00-index.adoc)

## similar projects:
- https://github.com/jjok/my-mopidy-setup
- https://github.com/wernight/docker-mopidy
- https://github.com/sweisgerber/docker-mopidy

***

## todo

- [ ] [Set up project integrations](https://gitlab.com/mcgr0g/mopidy-ams/-/settings/integrations)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
